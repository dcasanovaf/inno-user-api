# Innocv Rest User API

## Run the project
* To generate a jar executable java application run maven:package phase
* Also, to run the application you can run the main method defined in UserResource class

Be sure that the port **8080** is free

## REST curl request examples
### Method to get user by id
```
curl -X GET http://localhost:8080/api/user/get/1 
```

### Method to get all users
```
curl -X GET http://localhost:8080/api/user/getall
```

### Method to create an user
```
curl -X POST \
  http://localhost:8080/api/user/create \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
  -d '{
  "name": "Joao",
  "birthdate": "01-01-1980"
}'
```

### Method to update an existing user
```
curl -X POST \
  http://localhost:8080/api/user/update \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json'
  -d '{
  "id": 1,
  "name": "Falcon",
  "birthdate": "22-11-1984"
}'
```

### Method to remove user by id
```
curl -X GET \
  http://localhost:8080/api/user/remove/1
```



## Project technological stack
* Java 8
* Maven to manage dependencies
* spring-boot-maven-plugin to package executable jar
* spring-boot-starter-web to create the REST services and IOC
* spring-boot-starter-test to implementation of unitary and integration tests
* HSQLDB embbebed database
* Junit and Mockito
