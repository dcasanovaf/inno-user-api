package com.innocv.controller;

import com.innocv.model.User;
import com.innocv.exception.UserException;
import com.innocv.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserRestCtrl {
    private static final Logger log = LoggerFactory.getLogger(UserRestCtrl.class);

    @Autowired
    UserService userService;

    @RequestMapping(value = "/api/user/get/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public User getUser(@PathVariable("id") long id) throws UserException{
        log.info(new StringBuilder("requesting for user: ").append(id).toString());
        User user = userService.getUser(id);

        if(user == null){
            throw new UserException("User with id " + id + " has not been found");
        }

        return user;
    }

    @RequestMapping(value = "/api/user/getall", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<User> getAllUsers() throws UserException{
        log.info(new StringBuilder("requesting for all users").toString());
        return userService.getUsers();
    }

    @RequestMapping(value = "/api/user/create", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public User createUser(@RequestBody User user) throws UserException{
        log.info(new StringBuilder("requesting for create user").toString());
        return userService.addUser(user);
    }

    @RequestMapping(value = "/api/user/update", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public User updateUser(@RequestBody User user) throws UserException{
        log.info(new StringBuilder("requesting for update user").toString());

        User userToUpdate = userService.getUser(user.getId());

        if(userToUpdate == null){
            throw new UserException("User with id " + user.getId() + " has not been found");
        }

        return userService.updateUser(user);
    }

    @RequestMapping(value = "/api/user/remove/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeUser(@PathVariable("id") long id) throws UserException{
        log.info(new StringBuilder("requesting for remove user: ").append(id).toString());
        userService.deleteUser(id);
    }
}
