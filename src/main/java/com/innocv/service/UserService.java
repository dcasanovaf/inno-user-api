package com.innocv.service;

import com.innocv.model.User;

import java.util.List;

public interface UserService {

	User addUser(final User user);

	void deleteUser(final Long id);

	User updateUser(final User user);

	User getUser(final Long id);

	List<User> getUsers();
	
}
