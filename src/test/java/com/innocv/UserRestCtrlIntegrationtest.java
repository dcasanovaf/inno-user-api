package com.innocv;

import com.innocv.exception.UserException;
import com.innocv.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserRestCtrlIntegrationtest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getUserByIdTest() {
        ResponseEntity<User> responseEntity =
                restTemplate.getForEntity("/api/user/get/1", User.class);
        User user = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assert.notNull(user, "user is null");
    }

    @Test
    public void createUserTest() {
        ResponseEntity<User> responseEntity =
                restTemplate.postForEntity("/api/user/create", new User("Daniel", new Date()), User.class);
        User user = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assert.notNull(user, "user is null");
    }

    @Test
    public void getAllUsersTest() {
        ResponseEntity<List> responseEntity =
                restTemplate.getForEntity("/api/user/getall", List.class);
        List<User> users = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assert.notEmpty(users, "user is null");
    }

    @Test
    public void updateUserTest() {
        ResponseEntity<User> responseEntity =
                restTemplate.postForEntity("/api/user/update", new User(1L, "Joao", new Date()), User.class);
        User user = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assert.notNull(user, "user is null");
    }

}
