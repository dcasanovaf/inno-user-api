package com.innocv;

import com.innocv.dao.UserRepository;
import com.innocv.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByIDTest(){
        User user = userRepository.findOne(1L);
        Assert.notNull(user,"user is null");
    }

    @Test
    public void addTest(){
        User user = userRepository.save(new User());
        Assert.notNull(user,"user is null");
    }

    @Test
    public void findAllTest(){
        Iterable<User> users = userRepository.findAll();
        Assert.notNull(users,"user is null");
    }
}
